#include <stdio.h>
#include "projects.h"


int main() {
    
    int project_number;
    
    do {
        
        printf("Please enter a project number: ");
        
        scanf("%d", &project_number);
        
        
        switch (project_number) {
            
            //exit program
            case 0:
                printf("Exiting program");
                break;
                
                // Project 1 - Multiples of 3 and 5
            case 1:
                project_1();
                break;
                
                // Project 2 - Even Fibonacci numbers
            case 2:
                project_2();
                break;
                
                // Project 3 - Largest prime factor
            case 3:
                project_3();
                break;
        
                // Project 4 - Largest palindrome product
            case 4:
                project_4();
                break;
            
            default:
                printf("Invalid value\n");
        }
        
        printf("\n");
        
        
    } while (project_number != 0);
    
    return 0;
}