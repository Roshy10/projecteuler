//
// Created by roshan on 12/03/17.
//

int is_palindromic(int number);

int project_4() {
    
    int max_number = 0, current_number = 0, x, y;
    
    for (x = 100; x < 1000; x++) {
        for (y = 100; y < 1000; y++) {
            
            current_number = x * y;
            
            if (is_palindromic(current_number) == 1 && current_number > max_number) {
                max_number = current_number;
            }
        }
    }
    
    printf("The largest palindromic number is %d", max_number);
    
    return 0;
}

int is_palindromic(int n) {
    int reversedInteger = 0, remainder, originalInteger;
    
    originalInteger = n;
    
    // reversed integer is stored in variable
    while (n != 0) {
        remainder = n % 10;
        reversedInteger = reversedInteger * 10 + remainder;
        n /= 10;
    }
    
    // palindrome if orignalInteger and reversedInteger are equal
    if (originalInteger == reversedInteger)
        return 1;
    else
        return 0;
}