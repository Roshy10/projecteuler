//
// Created by roshan on 12/03/17.
//

int project_3() {
    long long num = 600851475143;
    
    for (int div = 2; div < num; div++) {
        if (num % div == 0) {
            num = num / div;
            div--;
        }
    }
    
    printf("The largest prime factor is %d", num);
    
    return 0;
}