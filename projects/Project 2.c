//
// Created by roshan on 10/03/17.
//

#include <stdio.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"


int project_2() {

    int sum = 0;
    int count = 0;
    int use_count = 0;
    int fibonacci[3] = {0, 1};


    while (fibonacci[1] < 4 * 1000 * 1000) {


        count++;

        if(fibonacci[1] % 2 ==0) {
            use_count++;
            sum += fibonacci[1];
            printf(ANSI_COLOR_GREEN "%d: %d" ANSI_COLOR_RESET "\n", count, fibonacci[1]);
        }else{
            printf(ANSI_COLOR_RED "%d: %d" ANSI_COLOR_RESET "\n", count, fibonacci[1]);
        }

        //increment array values to find nex number
        fibonacci[2] = fibonacci[0] + fibonacci[1];
        fibonacci[0] = fibonacci[1];
        fibonacci[1] = fibonacci[2];

    }
    printf("The sum is equal to %d\n", sum);
    printf("%d numbers, %d used", count, use_count);

    return 0;
}