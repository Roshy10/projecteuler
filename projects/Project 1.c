//
// Created by Roshan on 10/03/2017.
//
#include <stdio.h>

int project_1() {
    int sum = 0;
    int i;

    for (i = 0; i < 1000; ++i) {
        if( (i % 3 == 0) || (i % 5 == 0) ){
            sum += i;
        }
    }
    printf("The sum is equal to %d", sum);

    return 0;
}